import 'src/styles/globals.css';

import { useEffect } from 'react';
import { AppProps } from 'next/app';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { IntlProvider } from 'react-intl';

import { defaultLocale, locales } from 'src/localization';

require('src/migration').migration();

const MyApp = ({ Component, pageProps }: AppProps) => {
  const router = useRouter();
  useEffect(() => {
    router.push('/learn/regex101');
    
  }, []);

  const { metadata } = pageProps || {};
  const baseURL = process.env.NEXT_PUBLIC_BASE_URL;
  const { asPath } = useRouter();

  const href = asPath.replace('/es', '/').replace('//', '/');
  return (
    <IntlProvider
      messages={pageProps.messages}
      locale={pageProps.lang}
      defaultLocale={defaultLocale}
    >
     
      <div className="flex flex-col h-screen text-neutral-50 font-openSans">
        <Component {...pageProps} />
      </div>
    </IntlProvider>
  );
};

export default MyApp;
