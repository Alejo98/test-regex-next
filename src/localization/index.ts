export const langNames = {
  en: '🇺🇸',
  de: '🇩🇪',
  es: '🇪🇸',
  fr: '🇫🇷',
  tr: '🇹🇷',
  ru: '🇷🇺',
  'zh-cn': '🇨🇳',
  uk: '🇺🇦',
};

export const defaultLocale = 'es';

export const locales = Object.keys(langNames);
