import { FormattedMessage } from 'react-intl';
import cx from 'clsx';

import Icon from 'src/components/Icon';
import Logo from 'src/components/Logo';
import IntlLink from 'src/components/IntlLink';
import LanguageSelect from 'src/components/LanguageSelect';

import packageInfo from 'package.json';

interface Props {
  page?: 'home' | 'learn' | 'learn-detail' | 'cheatsheet' | 'playground';
}

const Header = ({ page }: Props) => {
  const isLearnDetail = page === 'learn-detail';
  const isPlaygroundPage = page === 'playground';

  return (
    <header
      className={cx('relative z-40 h-20', {
        'bg-neutral-800 px-4 border-b border-neutral-700': isPlaygroundPage,
      })}
    >
      <div className="flex items-center justify-center h-20">
       
        {isLearnDetail && <div id="ProgressArea" className="flex justify-center flex-1" />}
        <div className="flex flex-1 items-center text-sm justify-end gap-2 sm:gap-4">
          

         
        </div>
      </div>
    </header>
  );
};

export default Header;
