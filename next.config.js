/** @type {import('next').NextConfig} */

module.exports = {
  reactStrictMode: true,
  async rewrites() {
    return [
      {
        source: '/',
        destination: '/es',
      },
      {
        source: '/learn',
        destination: '/es/learn',
      },
      {
        source: '/learn/:lesson*',
        destination: '/es/learn/:lesson*',
      },
      {
        source: '/cheatsheet',
        destination: '/es/cheatsheet',
      },
      {
        source: '/playground',
        destination: '/es/playground',
      },
    ]
  },
  async redirects() {
    return [
      {
        source: '/es',
        destination: '/',
        permanent: true,
      },
      {
        source: '/es/learn',
        destination: '/learn',
        permanent: true,
      },
      {
        source: '/es/learn/:lesson*',
        destination: '/learn/:lesson*',
        permanent: true,
      },
      {
        source: '/es/cheatsheet',
        destination: '/cheatsheet',
        permanent: true,
      },
      {
        source: '/es/playground',
        destination: '/playground',
        permanent: true,
      },
    ]
  }
}